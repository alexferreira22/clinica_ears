<?php
	
if(!empty($_POST["form_name"] && $_POST["form_email"] && $_POST["form_subject"] && $_POST["form_message"])) {

	$nome = $_POST["form_name"]);
	$email = $_POST['form_email'];
	$assunto = $_POST["form_subject"];
	$telefone = $_POST["form_phone"];
	$mensagem = $_POST["form_message"];

	# Inicia a classe PHPMailer
	$mail = new PHPMailer();

	# Define os dados do servidor e tipo de conexão
	$mail->IsSMTP(); // Define que a mensagem será SMTP
	$mail->Host = 'stmp.'.SERVER; # Endereço do servidor SMTP
	$mail->Port = 587; // Porta TCP para a conexão
	$mail->SMTPAutoTLS = false; // Utiliza TLS Automaticamente se disponível
	$mail->SMTPAuth = true; # Usar autenticação SMTP - Sim
	$mail->SMTPSecure = false;  // SSL REQUERIDO pelo GMail
	$mail->Username = ''; # Usuário de e-mail
	$mail->Password = ''; // # Senha do usuário de e-mail
			
	# Define o remetente
	# Conta que fará o envio das mensagens (deve ser uma conta existente e ativa no seu domínio)
	$mail->Sender = '';
	$mail->From = $email;
	$mail->FromName = $nome; // Seu nome
	
	# Define os destinatário(s)
	$mail->AddAddress('ears@ears.com.br', 'Contato'); # Os campos podem ser substituidos por variáveis
	#$mail->AddAddress('webmaster@nomedoseudominio.com'); # Caso queira receber uma copia
	#$mail->AddCC('ciclano@site.net', 'Ciclano'); # Copia
	#$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); # Cópia Oculta

	# Define os dados técnicos da Mensagem
	$mail->IsHTML(true); # Define que o e-mail será enviado como HTML
	$mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)

	# Define a mensagem (Texto e Assunto)
	$mail->Subject = "Formulário de Contato - ".$nome; # Assunto da mensagem
	$corpo = '<table>';
	$corpo .= '<thead>';
	$corpo .= '<tr>';
	$corpo .= '<th>Nome</th><th>Email</th><th>Telefone</th><th>Assunto</th><th>Mensagem</th></tr></thead><tbody><tr>';
	$corpo .= '<td>'.$nome.'</td>';
	$corpo .= '<td>'.$email.'</td>';
	$corpo .= '<td>'.$telefone.'</td>';
	$corpo .= '<td>'.$assunto.'</td>';
	$corpo .= '<td>'.$mensagem.'</td>';
	$corpo .= '</tr></tbody></table>';
	$mail->Body = $corpo;

	# Envia o e-mail
	$enviado = $mail->Send();

	# Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

	# Exibe uma mensagem de resultado (opcional)
	if ($enviado) {
		return true;
	} else {
		return false;
	}

	} else {
		return false;
	}

?>