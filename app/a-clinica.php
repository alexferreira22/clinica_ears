<style type="text/css" media="screen">
	.item i {
	    color: #fff !important;
	    font-size: 12px !important;
	    top: 30px !important;
	    position: unset !important;
	}
	.linka {
	    background-color: #9d71ae !important;
	    color: #fff !important;
	    transition: 500ms ease-in;
	    font-size: 12px !important;
	}
	.owl-theme .owl-dots .owl-dot span {
		color: #9070aa !important;
	}
</style>
<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/banner-notfound.jpg">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">A Clínica</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li>
								<a href="#"> Sobre nós </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored"> A Clínica</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- DESCRICAO -->
	<div class="container pt-0 pb-0">
		<div class="section-content">
			<div class="row">
				<div class="col-md-6">
					<div class="border-left-2px border-theme-colored mt-60 pl-15">
						<p class="font-29 m-0 typo" style="color: #333333 !important;">Bem-vindo à </p>
						<h2 class="text-theme-colored mt-0 typo" style="color: #9070aa !important; font-size: 55px !important;">Clínica Ears</h2>
					</div>
					<p style="color: #767676 !important;">
						A EARS é uma clínica interdisciplinar voltada para a solução dos problemas auditivos e de comunicação. Disponibiliza atendimento diferenciado nas áreas de Otorrinolaringologia e Fonoaudiologia, com profissionais altamente especializados em atendimento médico, fonoterápico, psicológico e audiológico. Sempre em busca de tecnologias de ponta, conta com os mais modernos equipamentos, oferecendo diagnóstico e reabilitação de vanguarda no campo de audição e da fala.
					</p>
					<a class="btn btn-default btn-lg mt-10" href="/especialidades" style="color: #000 !important; font-weight: bold;">Especialidades</a>
				</div>
				<div class="col-sm-6 col-md-6">
					<img class="img-fullwidth" alt="" src="/assets/img/bg-clinica.jpg">
				</div>
			</div>
		</div>
	</div>
	<!-- SERVICOS -->
	<section>
		<div class="container pb-10">
			<div class="section-title mb-30">
				<div class="row">
					<div class="col-md-12">
						<h4 class="text-theme-colored title-border m-0 typo" style="font-size: 22px !important; color: #9070aa;">Serviços</h4>
					</div>
				</div>
			</div>
			<div class="section-content">
		      <div class="row features-style1 mt-20">
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/audicao.png" alt="Cuidados com a Sua Audição">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidados com a Sua Audição</h5>
		              <p>
		                Ouça melhor consultando com nossos profissionais qualificados.
		              </p>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/garganta.png" alt="Cuidados com a Sua Garganta">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidados com a Sua Garganta</h5>
		              <p>
		                Consulte com nossa equipe qualificada de otorrinolaringologistas.
		              </p>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/criancas.png" alt="Cuidado com suas crianças">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidado com suas crianças</h5>
		              <p>
		                A Ears Kids conta com um atendimento especializado para seus filhos.
		              </p>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/diccao.png" alt="Cuidados com a Sua Dicção">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidados com a Sua Dicção</h5>
		              <p>
		                Os fonoaudiólogos da Ears trabalham para que você consiga falar melhor
		              </p>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/pulmao.png" alt="Cuidados com a Sua Respiração">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidados com a Sua Respiração</h5>
		              <p>
		                Respire melhor consultando com nossos médicos.
		              </p>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-4">
		          <div class="icon-box left media p-0">
		            <a href="#" class="media-left pull-left">
		              <img src="/assets/img/cerebro.png" alt="Cuidados com a Sua Mente">
		            </a>
		            <div class="media-body">
		              <h5 class="media-heading heading">Cuidado com a Sua Mente</h5>
		              <p>
		                A Ears conta também com psicóloga para toda a família.
		              </p>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		</div>
	</section>
	<!-- Doutores -->
	<section>
		<div class="container pt-0 pb-80">
			<div class="section-title mb-30">
				<div class="row">
					<div class="col-md-12">
						<h4 class="text-theme-colored title-border m-0 typo" style="font-size: 22px !important; color: #9070aa;">Nossa equipe</h4>
					</div>
				</div>
			</div>
			<div class="section-content">
		      <div class="row multi-row-clearfix">
		        <div class="col-sm-6 mb-60 sm-text-center">
		          <div class="team maxwidth400">
		            <div class="thumb">
		              <img class="img-fullwidth" src="/assets/img/daniela.jpg" alt="Dra. Daniela Marques" title="Dra. Daniela Marques">
		            </div>
		            <div class="content border-1px p-15 bg-white-light">
		              <h4 class="name mb-0 mt-0">Dra<span>.</span> Daniela Marques</h4>
		              <h6 class="title mt-0">Fonoaudióloga</h6>
		              <p class="mb-30">Hospital de Clínicas de Porto Alegre, nas equipes de saúde auditiva – aparelhos auditivos e implantes cocleares.</p>
		              <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=daniela-marques">ver detalhes</a>
		            </div>
		          </div>
		        </div>
		        <div class="col-sm-6 mb-60 sm-text-center">
		          <div class="team maxwidth400">
		            <div class="thumb">
		              <img class="img-fullwidth" src="/assets/img/leticia.jpg" alt="Dra. Letícia Schmidt Rosito" title="Dra. Letícia Schmidt Rosito">
		            </div>
		            <div class="content border-1px p-15 bg-white-light">
		              <h4 class="name mb-0 mt-0">Dra<span>.</span> Letícia Schmidt Rosito</h4>
		              <h6 class="title mt-0">Otorrinolaringologista</h6>
		              <p class="mb-30">Mestre e doutora em cirurgia pela UFRGS. Áreas de atuação principais: otites, perdas auditivas, zumbidos e tonturas</p>
		              <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=leticia-schmidt">ver datalhes</a>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
		</div>
	</section>
	<!-- Funfact -->
    <section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.5" data-bg-img="/assets/img/banner-notfound.jpg">
      <div class="container pt-200 pb-120">
      </div>
    </section>
</div>
<?php
	$notFound = false;
?>