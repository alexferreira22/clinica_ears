<style type="text/css" media="screen">
	.media-body a:hover {
    	color: #999999 !important;
	}
	.styled-icons li a:hover {
		color: #fff !important;
	}
</style>
<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/banner-notfound.jpg">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">Contato</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored" style="color: #6dbfb1 !important;"> Contato</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- CONTATO -->
	<section class="inner-header">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 class="title typo">Contato</h2>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="#">Home</a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored" style="color: #6dbfb1 !important;">Contato</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FORM -->
	<section class="divider">
		<div class="container">
			<div class="row pt-30">
				<div class="col-md-12">
					<h3 class="line-bottom mt-0">Entre em contato com a gente!</h3>
					<ul class="styled-icons icon-dark icon-sm icon-circled mb-20">
						<li>
							<a href="#" data-bg-color="#3B5998" style="background: rgb(59, 89, 152) !important;">
								<i class="fab fa-facebook-f"></i> 
							</a>
						</li>
					</ul>

					<div class="icon-box media mb-15">
						<a class="media-left pull-left flip mr-20" href="#">
							<img src="/assets/img/localizacao.png" alt="Nosso Endereço">
						</a>
						<div class="media-body">
							<h5 class="mt-0 typo">Nosso Endereço</h5>
							<p>Rua Padre Chagas, 415, sala 501, Moinhos, Porto Alegre Rio Grande do Sul, Brasil</p>
						</div>
					</div>
					<div class="icon-box media mb-15">
						<a class="media-left pull-left flip mr-15" href="#">
							<img src="/assets/img/phone.png" alt="Nossos Números">
						</a>
						<div class="media-body">
							<h5 class="mt-0">Nossos Números</h5>
							<p><a href="tel:+555132074284">+(55) 51 3207-4284</a><!--&nbsp;|&nbsp;<a href="tel:+325-12345-65478">(51) 55  9999 9999</a>--></p>
						</div>
					</div>
					<div class="icon-box media mb-15">
						<a class="media-left pull-left flip mr-15" href="#">
							<img src="/assets/img/message.png" alt="Nosso Email">
						</a>
						<div class="media-body">
							<h5 class="mt-0">Nosso Email</h5>
							<p><a href="mailto:ears@ears.com.br">ears@ears.com.br</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- MAPS -->
	<section>
		<div class="container-fluid pt-0 pb-0">
			<div class="row">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.4484840722243!2d-51.20401458488533!3d-30.023988781889766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951979b7b6601469%3A0x716573a45eecdc1!2sR.+Padre+Chagas%2C+415+-+Moinhos+de+Vento%2C+Porto+Alegre+-+RS%2C+90570-080!5e0!3m2!1spt-BR!2sbr!4v1548772161711" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
</div>
<?php
	$notFound = false;
?>