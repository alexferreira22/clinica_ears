<?php
	if(isset($_GET["nome"])) {
	$nome = filtroGetPost($_GET["nome"]);
	$doutores = array(
		"daniela-marques" => array (
			"Nome" => "Dra. Daniela Marques",
			"Foto" => "IMG_9963.jpg",
			"Descricao" => "Mestre em Psicologia pela Universidade Federal do Rio Grande do Sul (UFRGS)<br>
							Doutora em Linguística Aplicada pela UFRGS<br>
							Fonoaudióloga do Hospital de Clínicas de Porto Alegre (HCPA)<br>
							Especialista em Fonoaudiologia Hospitalar pelo Centro Educacional São Camilo<br>
							Coordenadora do Programa de Capacitação em Saúde Auditiva - PROSA<bR>
							Participante da equipe de implante coclear do HCPA<br>
							Atua nas áreas de saúde auditiva - aparelhos auditivos, implantes cocleares e na reabilitação dos distúrbios da linguagem e da fala.
",
			"Funcao" => "Fonoaudióloga - CRFa RS 8177",
			"Detalhes" => "/doutores?nome=daniela-marques"
		),
		"leticia-schmidt" => array (
			"Nome" => "Dra. Letícia Schmidt Rosito",
			"Foto" => "IMG_9897.jpg",
			"Descricao" => "Mestre e doutora em cirurgia pela UFRGS<br>
							Professora do Departamento de otorrinolaringologia da UFRGS<br>
							Coordenadora do ambulatório de zumbido do HCPA<br>
							Coordenadora do ambulatório de surdez infantil do HCPA<br>
							Participante da equipe de implante coclear do HCPA<br>
							Áreas de atuação principais: otites, perdas auditivas, zumbidos e tonturas.",
			"Funcao" => "Otorrinolaringologista - CRM RS 26451",
			"Detalhes" => "/doutores?nome=leticia-schmidt"
		)
	);
?>
	<script type="text/javascript">
		document.title = <?php echo '"'.$doutores[$nome]["Nome"].' - Ears"'; ?>
	</script>
	<style>
	.breadcrumb.white li.active {
		color: #6dbfb1 !important;
	}
	.section-content dt {
		font-family: 'TypoRoundLight', sans-serif !important;
	}
	.title-border::before {
		background-color: #6dbfb1 !important;
	}
	li {
		list-style: none;
	}
</style>
<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/banner-notfound.jpg">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">A Clínica</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li>
								<a href="/nossa-equipe"> Nossa equipe </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored"> <?php echo $doutores[$nome]["Nome"]; ?></li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- SOBRE -->
	<section style="padding-bottom: 50px;">
		<div class="container">
			<div class="section-content">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8 pull-right pl-60 pl-sm-15">
						<div>
							<h3><?php echo $doutores[$nome]["Nome"]; ?></h3>
							<h5 class="text-theme-colored" style="color: #6dbfb1 !important;"><?php echo $doutores[$nome]["Funcao"]; ?></h5>
							<p>
								<?php echo $doutores[$nome]["Descricao"]; ?>
							</p>
						</div>
					</div>
					<div class="col-sx-12 col-sm-4 col-md-4 sidebar pull-left">
						<div class="doctor-thumb">
							<img src="/assets/img/equipe/<?php echo $doutores[$nome]["Foto"]; ?>" alt="<?php echo $doutores[$nome]["Nome"]; ?>" title="<?php echo $doutores[$nome]["Nome"]; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
	<?php
		} else {
	?>
	<div class="main-content">
		<!-- BANNER -->
		<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/banner-notfound.jpg">
			<div class="container pt-90 pb-50">
				<div class="section-content">
					<div class="row"> 
						<div class="col-md-12 xs-text-center pt-90">
							<h3 class="font-29 typo">Doutor não informado!</h3>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php
		}
		$notFound = false;
	?>