<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/eras-kids.jpg">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">Ears Kids</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored"> Ears Kids</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
  <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="portfolio-filter font-alt align-center">
              <h4>“A Clínica possui um Espaço Kids destinado aos pequenos com brinquedos, livros e muita interatividade”</h4><br>
            </div>
            <div id="grid" class="gallery-isotope grid-2 masonry gutter clearfix">
              <?php
                for($i = 1; $i < 16; $i++) {
              ?>
                <div class="gallery-item photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="/assets/img/kids/<?=$i;?>.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="icons-holder">
                    </div>
                    <a class="hover-link" data-lightbox="image" href="/assets/img/kids/1.jpg">View more</a>
                  </div>
                </div>
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<?php
	$notFound = false;
?>