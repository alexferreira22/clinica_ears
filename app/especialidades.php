<?php
  $servico = addslashes($_GET["servico"]);
  switch ($servico) {
    case 'otorrinolaringologicos':
    $otorrinolaringologicos = true;
    $title = "Otorrinolaringológicos - Ears";
    break;
    case 'fonoaudiologicos':
    $fonoaudiologicos = true;
    $title = "Fonoaudiológicos - Ears";
    break;
    case 'psicologicos':
    $psicologicos = true;
    $title = "Psicológicos - Ears";
    break;
    default:
    $otorrinolaringologicos = true;
    $title = "Especialidades - Ears";
    break;
}
$doutores = array(
    0 => array (
      "Nome" => "Dra. Daniela Marques",
      "Foto" => "IMG_9963.jpg",
      "Descricao" => "Mestre e doutora pela UFRGS. Fonoaudióloga do Hospital de Clínicas de Porto Alegre (HCPA). Atua nas áreas de saúde auditiva - aparelhos auditivos, implantes cocleares e na reabilitação dos distúrbios da linguagem e da fala.",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=daniela-marques"
    ),
    1 => array (
      "Nome" => "Dra. Letícia Schmidt Rosito",
      "Foto" => "IMG_9897.jpg",
      "Descricao" => "Mestre e doutora pela UFRGS. Coordenadora do ambulatório do zumbido e de surdez infantil do HCPA. Professora do Departamento de Otorrinolaringologia da UFRGS. Tem como principais áreas de atuação: otites, perdas auditivas, zumbidos e tonturas.",
      "Funcao" => "Otorrinolaringologista",
      "Detalhes" => "/doutores?nome=leticia-schmidt"
    ),
  );
?>
<script type="text/javascript">
  document.title = <?php echo '"'.$title.'"'; ?>
</script>
<style type="text/css" media="screen">
  .breadcrumb.white li.active {
    color: #6dbfb1 !important;
  }
  .item i {
      color: #fff !important;
      font-size: 12px !important;
      top: 30px !important;
      position: unset !important;
  }
  .linka {
      background-color: #6dbfb1 !important;
      color: #fff !important;
      transition: 500ms ease-in;
      font-size: 12px !important;
  }
  .title-border::before {
    background-color: #6dbfb1 !important;
  }
  .bg-icon-doutores a {
    background: #6dbfb1 !important;
  }
  .footer-box-one > div {
    background: #6dbfb1 !important;
  }
  .footer-box-two > div {
    background: #4b898c !important;
  }
  .footer-box-three > div {
    background: #6dbfb1 !important;
  }
</style>
<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/p4.png">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">Especialidades</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored"> Especialidades</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
  <!-- Especialidades -->
  <section class="especialidades divider" style="background-color: #f1f1f1 !important;">
    <div class="container pb-80">
      <div class="section-title text-center">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <h2 class="text-uppercase mt-0">Nossas Especialidades</h2>
            <div class="title-icon">
              <img class="mb-10" src="/assets/img/icon-especialidades.png" alt="">
            </div>
            <p>
              Nossos profissionais, além de qualificados, são especialistas<br>em atender com carinho e dedicação
            </p>
          </div>
        </div>
      </div>
      <div class="section-centent">
        <div class="row">
          <div class="col-md-12">
            <div class="services-tab border-10px bg-white-light">
              <ul class="nav nav-tabs">
                <li <?php echo (isset($otorrinolaringologicos) && $otorrinolaringologicos == true) ? 'class="active otorrinolaringologia"' : 'class="otorrinolaringologia"'; ?>>
                  <a href="#tab11" data-toggle="tab">
                    <img src="/assets/img/otorrinolaringologicos.png" class="mg-auto pdg-bottom15 img-oto" alt="Otorrinolaringologia">Otorrinolaringologia</a>
                  </li>
                  <li <?php echo (isset($fonoaudiologicos) && $fonoaudiologicos == true) ? 'class="active fonoaudiologia"' : 'class="fonoaudiologia"'; ?>>
                    <a href="#tab12" data-toggle="tab">
                      <img src="/assets/img/fono.png" class="mg-auto pdg-bottom15 img-fono" alt="Fonoaudiologia">Fonoaudiologia
                    </a>
                  </li>
                  <li <?php echo (isset($psicologicos) && $psicologicos == true) ? 'class="active psicologia"' : 'class="psicologia"'; ?>>
                    <a href="#tab13" data-toggle="tab">
                      <img src="/assets/img/psicologia.png" class="mg-auto pdg-bottom15 img-psico" alt="Psicologia">Psicologia
                    </a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div <?php echo (isset($otorrinolaringologicos) && $otorrinolaringologicos == true) ? 'class="tab-pane fade in active"' : 'class="tab-pane fade in"'; ?> id="tab11">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <div class="thumb-otorrinolaringologia"></div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                          <h1 class="title mt-0">Otorrinolaringológicos</h1>
                          <p>
                            Tratamos pacientes de todas as idades com problemas de infecção de ouvido, problemas nas vias aéreas superiores como rinites, otites, sinusites, amigdalites e faringites. Também lidamos com doenças crônicas como surdez, labirintites e zumbido, obstrução nasal, roncos e apnéia do sono, dificuldade de deglutição entre outros.
                          </p>
                          <div class="row mt-30 mb-20">
                           <div class="col-xs-6">
                            <ul class="mt-10">
                              <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Letícia Schmidt Rosito</li>
                              <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dr. Rafael Malinsky</li>
                              <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp; Dr. Airton Malinsky</li>
                            </ul>
                          </div>
                          <div class="col-xs-6">
                            <ul class="mt-10">
                              <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Denise Manica</li>
                              <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Caroline Elias</li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div <?php echo (isset($fonoaudiologicos) && $fonoaudiologicos == true) ? 'class="tab-pane fade in active"' : 'class="tab-pane fade in"'; ?> id="tab12">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="thumb">
                        <div class="thumb-fonoaudiologicos"></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="service-content">
                        <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                        <h1 class="title mt-0">Fonoaudiológicos</h1>
                        <p>
                          <b>Habilitação e reabilitação fonoaudiológica nas áreas de:</b><br>
                          Audição, 
                          equilíbrio, 
                          fala, 
                          linguagem, 
                          voz, 
                          motricidade oral e 
                          disfagia. 
                          <br>
                          <b>Realizamos os exames audiológicos:</b><br>
                          audiometria tonal e vocal, 
                          imitanciometria, 
                          emissões otoacústicas,
                          potencial evocado auditivo de tronco encefálico (PEATE), 
                          potencial evocado auditivo de estado estável, 
                          videonistagmografia, 
                          posturografia, 
                          seleção e adaptação de aparelhos auditivos, 
                          videofluoroscopia, 
                          Mapeamento de implantes cocleares.
                        </p>
                        <div class="row mt-30 mb-20">
                         <div class="col-xs-6">
                          <ul class="mt-10">
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Daniela Marques</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Lucia Pauletti</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Luciana Netto</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Maria Elisa Braga</li>
                          </ul>
                        </div>
                        <div class="col-xs-6">
                          <ul class="mt-10">
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Lucia Geyer</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Cristine Alves</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Daniel Marchand</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Alana Signorini</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div <?php echo (isset($psicologicos) && $psicologicos == true) ? 'class="tab-pane fade in active"' : 'class="tab-pane fade in"'; ?> id="tab13">
                <div class="row">
                  <div class="col-md-5">
                    <div class="thumb">
                      <div class="thumb-psicologia"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="service-content">
                      <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                      <h1 class="title mt-0">Psicológicos</h1>
                      <p>
                        Realizamos atendimentos psicológicos a pacientes e seus familiares.
                      </p>
                      <div class="row mt-30 mb-20">
                       <div class="col-xs-6">
                        <ul class="mt-10">
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Letícia Bulcão</li>
                        </ul>
                      </div>
                    </div>
                    <p>
                      Segunda à sexta, das 8h às 20h.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.5" data-bg-img="/assets/img/especialidades.jpg" style="background-position: 50% -141.5px !important;">
        <div class="container pt-200 pb-120">
        </div>
      </section>
<!-- Doutores -->
<br><br>
  <section>
    <div class="container pt-0 pb-80">
      <div class="section-title mb-30">
        <div class="row">
          <div class="col-md-12">
            <h4 class="text-theme-colored title-border m-0 typo" style="font-size: 22px !important; color: #9070aa;">Nossa equipe</h4>
          </div>
        </div>
      </div>
      <div class="section-content">
          <div class="row multi-row-clearfix">
            <div class="col-sm-6 mb-60 sm-text-center">
              <div class="team maxwidth400">
                <div class="thumb">
                  <img class="img-fullwidth" src="/assets/img/daniela.jpg" alt="Dra. Daniela Marques" title="Dra. Daniela Marques">
                </div>
                <div class="content border-1px p-15 bg-white-light">
                  <h4 class="name mb-0 mt-0">Dra<span>.</span> Daniela Marques</h4>
                  <h6 class="title mt-0">Fonoaudióloga</h6>
                  <p class="mb-30">Hospital de Clínicas de Porto Alegre, nas equipes de saúde auditiva – aparelhos auditivos e implantes cocleares.</p>
                  <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=daniela-marques">ver detalhes</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 mb-60 sm-text-center">
              <div class="team maxwidth400">
                <div class="thumb">
                  <img class="img-fullwidth" src="/assets/img/leticia.jpg" alt="Dra. Letícia Schmidt Rosito" title="Dra. Letícia Schmidt Rosito">
                </div>
                <div class="content border-1px p-15 bg-white-light">
                  <h4 class="name mb-0 mt-0">Dra<span>.</span> Letícia Schmidt Rosito</h4>
                  <h6 class="title mt-0">Otorrinolaringologista</h6>
                  <p class="mb-30">Mestre e doutora em cirurgia pela UFRGS. Áreas de atuação principais: otites, perdas auditivas, zumbidos e tonturas</p>
                  <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=leticia-schmidt">ver datalhes</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
      <!-- Section: funfact -->
      <section class="divider parallax layer-overlay overlay-light" data-stellar-background-ratio="0.5" data-bg-img="/assets/img/banner-notfound.jpg">
        <div class="container pt-200 pb-120">
        </div>
      </section>
    </div>
<?php
  $notFound = false;
?>