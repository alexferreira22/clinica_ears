<style type="text/css" media="screen"> 
  .owl-prev i {
    color: #375e5f !important;
      border: 1px solid #375e5f;
      padding-left: 10px;
      padding-right: 5px;
      padding-top: 5px;
      padding-bottom: 5px;
      font-size: 18px !important;
  }
  .owl-next i {
    color: #375e5f !important;
      border: 1px solid #375e5f;
      padding-left: 5px;
      padding-right: 10px;
      padding-top: 5px;
      padding-bottom: 5px;
      font-size: 18px !important;
  }  
</style>
<!-- Quem Somos -->  
<section>
  <div class="container pb-0 pt-110">
    <div class="row equal-height-inner">
      <div class="col-md-4 mb-sm-60">
        <div class="feature-icon-box p-30">
          <div class="feature-icon bg-about">
            <i class="fa fa-heartbeat text-white font-30"></i> 
          </div>
          <h6 class="text-uppercase">Quem somos</h6>
          <h4 class="text-uppercase">A Ears</h4>
          <p class="description-p">
            A EARS é uma clínica interdisciplinar voltada para a solução dos problemas auditivos e de comunicação. Disponibiliza atendimento diferenciado nas áreas de Otorrinolaringologia e Fonoaudiologia.
          </p>
          <a href="/sobre-nos" class="btn p-0 mt-15 font-13 btn-about">Saiba mais <i class="fa fa-angle-double-right ml-5"></i></a>
        </div>
      </div>
      <div class="col-md-4 mb-sm-60">
        <div class="feature-icon-box p-30">
          <div class="feature-icon bg-about">
            <i class="fa fa-user-md text-white font-30"></i> 
          </div>
          <h6 class="text-uppercase">Nossa Equipe</h6>
          <h4 class="text-uppercase">Diferentes Especialidades</h4>
          <p class="description-p">
            Nosso foco principal são as doenças do ouvido - surdez, otites, zumbido, vertigem, desenvolvimento de linguagem - além de otorrinolaringologia e fonoaudiologia em geral, atendendo crianças e adultos.
          </p> 
          <a href="/nossa-equipe" class="btn p-0 mt-15 font-13 btn-about">Saiba mais <i class="fa fa-angle-double-right ml-5"></i></a>
        </div>
      </div>
      <div class="col-md-4">
        <div class="feature-icon-box p-30 pb-0">
          <div class="feature-icon bg-about">
            <i class="fa fa-clock text-white font-30"></i>
          </div>
          <h4 class="text-uppercase mb-5">Horários de Funcionamento</h4>
          <div class="widget mb-0">
            <div class="opening-hourse">
              <ul class="list-unstyled">
                <li class="clearfix"> <span> Segunda - Sexta </span>
                  <div class="value"> 9h às 20h </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Servicos -->
<section class="text-center services">
  <div class="container pb-10">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h2 class="text-uppercase mt-0">Serviços</h2>
          <div class="title-icon">
            <img class="mb-10" src="/assets/img/icon-service.png" alt="">
          </div>
          <p class="bold">
            Na Ears você conta com diversos serviços para<br>ouvir e falar melhor
          </p>
        </div>
      </div>
    </div>
    <div class="section-content">
      <div class="row features-style1 mt-20">
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/audicao.png" alt="Cuidados com a Sua Audição">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidados com a Sua Audição</h5>
              <p>
                Ouça melhor consultando com nossos profissionais qualificados.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/garganta.png" alt="Cuidados com a Sua Garganta">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidados com a Sua Garganta</h5>
              <p>
                Consulte com nossa equipe qualificada de otorrinolaringologistas.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/criancas.png" alt="Cuidado com suas crianças">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidado com suas crianças</h5>
              <p>
                A Ears Kids conta com um atendimento especializado para seus filhos.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/diccao.png" alt="Cuidados com a Sua Dicção">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidados com a Sua Dicção</h5>
              <p>
                Os fonoaudiólogos da Ears trabalham para que você consiga falar melhor
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/pulmao.png" alt="Cuidados com a Sua Respiração">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidados com a Sua Respiração</h5>
              <p>
                Respire melhor consultando com nossos médicos.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon-box left media p-0">
            <a href="#" class="media-left pull-left">
              <img src="/assets/img/cerebro.png" alt="Cuidados com a Sua Mente">
            </a>
            <div class="media-body">
              <h5 class="media-heading heading">Cuidado com a Sua Mente</h5>
              <p>
                A Ears conta também com psicóloga para toda a família.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Especialidades -->
<section class="especialidades divider parallax layer-overlay overlay-deep" data-bg-img="/assets/img/bg-especialidade.jpg">
  <div class="container pb-80">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h2 class="text-uppercase mt-0">Nossas Especialidades</h2>
          <div class="title-icon">
            <img class="mb-10" src="/assets/img/icon-especialidades.png" alt="">
          </div>
          <p>
            Nossos profissionais, além de qualificados, são especialistas<br>em atender com carinho e dedicação
          </p>
        </div>
      </div>
    </div>
    <div class="section-centent">
      <div class="row">
        <div class="col-md-12">
          <div class="services-tab border-10px bg-white-light">
            <ul class="nav nav-tabs">
              <li class="active otorrinolaringologia">
                <a href="#tab11" data-toggle="tab">
                  <img src="/assets/img/otorrinolaringologicos.png" class="mg-auto pdg-bottom15 img-oto" alt="Otorrinolaringologia">Otorrinolaringologia</a>
                </li>
                <li class="fonoaudiologia">
                  <a href="#tab12" data-toggle="tab">
                    <img src="/assets/img/fono.png" class="mg-auto pdg-bottom15 img-fono" alt="Fonoaudiologia">Fonoaudiologia
                  </a>
                </li>
                <li clas="psicologia">
                  <a href="#tab13" data-toggle="tab">
                    <img src="/assets/img/psicologia.png" class="mg-auto pdg-bottom15 img-psico" alt="Psicologia">Psicologia
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade in active" id="tab11">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="thumb">
                        <div class="thumb-otorrinolaringologia"></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="service-content">
                        <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                        <h1 class="title mt-0">Otorrinolaringológicos</h1>
                        <p>
                          Tratamos pacientes de todas as idades com problemas de infecção de ouvido, problemas nas vias aéreas superiores como rinites, otites, sinusites, amigdalites e faringites. Também lidamos com doenças crônicas como surdez, labirintites e zumbido, obstrução nasal, roncos e apnéia do sono, dificuldade de deglutição entre outros.
                        </p>
                        <div class="row mt-30 mb-20">
                         <div class="col-xs-6">
                          <ul class="mt-10">
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Letícia Schmidt Rosito</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Denise Manica</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Caroline Catherine Lacerda Elias</li>
                          </ul>
                        </div>
                        <div class="col-xs-6">
                          <ul class="mt-10">
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dr. Rafael Malinsky</li>
                            <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dr. Airton Malinsky</li>
                          </ul>
                        </div>
                      </div>
                      <a class="btn btn-lg btn-services" href="/especialidades?servico=otorrinolaringologicos">Saiba mais</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tab12">
                <div class="row">
                  <div class="col-md-5">
                    <div class="thumb">
                      <div class="thumb-fonoaudiologicos"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="service-content">
                      <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                      <h1 class="title mt-0">Fonoaudiológicos</h1>
                      <p>
                        <b>Habilitação e reabilitação fonoaudiológica nas áreas de:</b><br>
                        Audição, 
                        equilíbrio, 
                        fala, 
                        linguagem, 
                        voz, 
                        motricidade oral e 
                        disfagia. 
                        <br>
                        <b>Realizamos os exames audiológicos:</b><br>
                        audiometria tonal e vocal, 
                        imitanciometria, 
                        emissões otoacústicas,
                        potencial evocado auditivo de tronco encefálico (PEATE), 
                        potencial evocado auditivo de estado estável, 
                        videonistagmografia, 
                        posturografia, 
                        seleção e adaptação de aparelhos auditivos, 
                        videofluoroscopia, 
                        Mapeamento de implantes cocleares.
                      </p>
                      <div class="row mt-30 mb-20">
                       <div class="col-xs-6">
                        <ul class="mt-10">
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Daniela Marques</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Cristine Alves</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Maria Elisa Braga</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Lucia Geyer</li>
                        </ul>
                      </div>
                      <div class="col-xs-6">
                        <ul class="mt-10">
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Pâmela Panassol</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Luciana Netto</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dr. Daniel Marchand</li>
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Alana Signorini</li>
                        </ul>
                      </div>
                    </div>
                    <a class="btn btn-lg btn-services" href="/especialidades?servico=fonoaudiologicos">Saiba Mais</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="tab13">
              <div class="row">
                <div class="col-md-5">
                  <div class="thumb">
                    <div class="thumb-psicologia"></div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="service-content">
                    <h3 class="sub-title mb-0 mt-30">Serviços</h3>
                    <h1 class="title mt-0">Psicológicos</h1>
                    <p>
                      Realizamos atendimentos psicológicos a pacientes e seus familiares.
                    </p>
                    <div class="row mt-30 mb-20">
                     <div class="col-xs-6">
                        <ul class="mt-10">
                          <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Dra. Letícia S. Abascal Bulcão</li>
                        </ul>
                     </div>
                    </div>
                    <p>
                      Segunda à sexta, das 8h às 20h.
                    </p>
                  <a class="btn btn-lg btn-services" href="/especialidades?servico=psicologicos">Saiba Mais</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<!-- Nossa Clinica -->
<section>
  <div class="container-fluid pt-0 pb-0">
    <div class="row equal-height">
      <div class="col-md-4 bg-img-cover sm-height-auto" data-bg-img="/assets/img/img4.jpg"></div>
      <div class="col-md-4 bg-clinica-home sm-height-auto">
        <div class="p-60">
          <h3 class="text-white mb-20 mt-0">Nossa Clínica</h3>
          <ul class="list list-white check font-15">
            <li>
              <i class="fas fa-check"></i>
              <a class="text-white" href="#">Cinco Consultórios</a>
            </li>
            <li>
              <i class="fas fa-check"></i>
              <a href="#">Espaço Kids</a>
            </li>
            <li>
              <i class="fas fa-check"></i>
              <a href="#">Climatização</a>
            </li>
            <li>
              <i class="fas fa-check"></i>
              <a href="#">Boa Localização</a>
            </li>
            <li>
              <i class="fas fa-check"></i>
              <a href="#">Sala de Espera Ampla</a>
            </li>
            <li>
              <i class="fas fa-check"></i>
              <a href="#">Salas Confortáveis</a>
            </li>
          </ul>
          <a href="/nossa-equipe" class="btn btn-default btn-lg btn-circled mt-20">ver mais</a>
        </div>
      </div>
      <div class="col-md-4 p-0 bg-img-cover sm-height-auto" data-bg-img="/assets/img/bg-contato.png" style="background-attachment: fixed;">
        <div class=" mb-sm-60 p-60">
          <h3 class="mb-20 mt-0">Contato</h3>
          <ul class="list list-white check font-15 list-clinica">
            <li>
              <img src="/assets/img/localizacao.png" alt="Nosso Endereço">
              <span>Nosso Endereço</span>
              <p>Rua Padre Chagas, 415, sala 501, Moinhos,Porto Alegre Rio Grande do Sul, Brasil</p>
            </li>
            <li>
              <img src="/assets/img/phone.png" alt="Nossos Números">
              <span>Nossos Números</span>
              <p><a href="tel:+555132074284" class="list-link">+ (55) 51 3207-4284</a></p>
            </li>
            <li>
              <img src="/assets/img/message.png" alt="Nosso Email">
              <span>Nosso Email</span>
              <p><a href="mailto:ears@ears.com.br" class="list-link">ears@ears.com.br</a></p>
            </li>
            <li>
              <a href="/contato" class="btn btn-default btn-lg btn-circled btn-clinica-home mt-20">mande uma mensagem</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- NOSSA EQUIPE -->
<section class="services doutores">
  <div class="container pb-20">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h2 class="text-uppercase mt-0">Nossa Equipe</h2>
          <div class="title-icon">
            <img class="mb-10" src="/assets/img/icon-service.png" alt="">
          </div>
          <p>A Clínica Ears conta com uma equipe qualificada<br> de 15 profissionais de diferentes especialidades</p>
        </div>
      </div>
    </div>
    <div class="section-content">
      <div class="row multi-row-clearfix">
        <div class="col-sm-6 mb-60 sm-text-center">
          <div class="team maxwidth400">
            <div class="thumb">
              <img class="img-fullwidth" src="/assets/img/daniela.jpg" alt="Dra. Daniela Marques" title="Dra. Daniela Marques">
            </div>
            <div class="content border-1px p-15 bg-white-light">
              <h4 class="name mb-0 mt-0">Dra<span>.</span> Daniela Marques</h4>
              <h6 class="title mt-0">Fonoaudióloga</h6>
              <p class="mb-30">Hospital de Clínicas de Porto Alegre, nas equipes de saúde auditiva – aparelhos auditivos e implantes cocleares.</p>
              <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=daniela-marques">ver detalhes</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 mb-60 sm-text-center">
          <div class="team maxwidth400">
            <div class="thumb">
              <img class="img-fullwidth" src="/assets/img/leticia.jpg" alt="Dra. Letícia Schmidt Rosito" title="Dra. Letícia Schmidt Rosito">
            </div>
            <div class="content border-1px p-15 bg-white-light">
              <h4 class="name mb-0 mt-0">Dra<span>.</span> Letícia Schmidt Rosito</h4>
              <h6 class="title mt-0">Otorrinolaringologista</h6>
              <p class="mb-30">Mestre e doutora em cirurgia pela UFRGS. Áreas de atuação principais: otites, perdas auditivas, zumbidos e tonturas</p>
              <a class="btn linka btn-sm pull-right flip" href="/doutores?nome=leticia-schmidt">ver datalhes</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
   $notFound = false;
?>