<?php
  $doutores = array(
    0 => array (
      "Nome" => "Dra. Daniela Marques",
      "Foto" => "IMG_9963.jpg",
      "Descricao" => "Mestre e doutora pela UFRGS. Fonoaudióloga do Hospital de Clínicas de Porto Alegre (HCPA). Atua nas áreas de saúde auditiva - aparelhos auditivos, implantes cocleares e na reabilitação dos distúrbios da linguagem e da fala.<br><br>",
      "Funcao" => "Fonoaudióloga - CRFa RS 8177",
      "Detalhes" => "/doutores?nome=daniela-marques"
    ),
    1 => array (
      "Nome" => "Dra. Letícia Schmidt Rosito",
      "Foto" => "IMG_9897.jpg",
      "Descricao" => "Mestre e doutora pela UFRGS. Coordenadora do ambulatório do zumbido e de surdez infantil do HCPA. Professora do Departamento de Otorrinolaringologia da UFRGS. Tem como principais áreas de atuação: otites, perdas auditivas, zumbidos e tonturas.",
      "Funcao" => "Otorrinolaringologista - CRM RS 26451",
      "Detalhes" => "/doutores?nome=leticia-schmidt"
    ),
    2  => array (
      "Nome" => "Dr. Rafael Malinsky",
      "Foto" => "8.jpg",
      "Descricao" => "Doutor em Ciências Médicas pela Universidade de São Paulo (USP). Observership em Sleep Surgery pela Universidade Stanford, nos Estados Unidos. Preceptor e responsável pelo ambulatório de Apneia do Sono no Serviço de Otorrinolaringologia da ULBRA. Atua nas áreas de ronco, apnéia do sono, nariz e seios da face.",
      "Funcao" => "Otorrinolaringologista",
      "Detalhes" => "/doutores?nome=rafael-malinsky"
    ),
    9  => array (
      "Nome" => "Pâmela Panassol",
      "Foto" => "IMG_0212.jpg",
      "Descricao" => "Possui aperfeiçoamento em Audiologia pelo HCPA, especialização em Audiologia pela UFRGS e aprimoramento em Processamento Auditivo. Atualmente realiza avaliação audiológica adulto e infantil, avaliação do processamento auditivo central e seleção e adaptação de aparelhos auditivos.",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=pamela-panassol"
    ),
    10  => array (
      "Nome" => "Dra. Cristine Alves",
      "Foto" => "IMG_0235.jpg",
      "Descricao" => "Profissional com mais de quinze anos de atuação, realiza avaliação audiológica adulto e infantil, exames ocupacionais, reabilitação vestibular e ministra palestras em empresas sobre saúde auditiva.<br><br>",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=cristiane-alves"
    ),
    6  => array (
      "Nome" => "Luciane F. Pauletti",
      "Foto" => "5.jpg",
      "Descricao" => "Especialista em Fonoaudiologia com ênfase na Infância pela UFRGS. Preceptora local de estágio do Curso de Fonoaudiologia da Universidade Federal de Ciências da Saúde de Porto Alegre (UFCSPA). Realiza exames audiológicos eletrofisiológicos (BERA) e seleção e adaptação de aparelhos auditivos.",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=luciane-pauletti"
    ),
    7  => array (
      "Nome" => "Luciana Netto",
      "Foto" => "6.jpg",
      "Descricao" => "Mestre em Pediatria pela UFRGS e especialista em fonoaudiologia pela Universidade Estácio de Sá. Fellowship em Audiologia pelo HCPA. Atua nas áreas de avaliação audiológica infantil, ativação e mapeamento de implante coclear e de próteses auditivas ancoradas no osso. ",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=luciana-netto"
    ),
    8  => array (
      "Nome" => "Maria Elisa Braga",
      "Foto" => "7.jpg",
      "Descricao" => "Mestre em Pediatria pela UFRGS. Especialista em Audiologia pelo Centro de Estudos dos Distúrbios da Audição (CEDIAU). Possui foco nas áreas de avaliação audiológica infantil, ativação e mapeamento de implante coclear e de próteses auditivas ancoradas no osso.",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=maria-elisa"
    ),
    4  => array (
      "Nome" => "Dra. Denise Manica",
      "Foto" => "1.jpg",
      "Descricao" => "Mestre e Doutora em Pediatria pela UFRGS. Fellowship em Laringe e preceptora em Otorrinolaringologia pediátrica no HCPA. Atua na área de otorrinolaringologia pediátrica geral, com foco em via aérea pediátrica.",
      "Funcao" => "Otorrinolaringologista",
      "Detalhes" => "/doutores?nome=denise-manica"
    ),
    11  => array (
      "Nome" => "Daniel Lucas Picanço Marchand",
      "Foto" => "2.jpg",
      "Descricao" => "Mestre e Doutor em Ciências da Reabilitação pela UFCSPA. Especialização em voz pelo Centro de Estudos da Voz (CEV).Professor no curso de Fonoaudiologia na Faculdade Fátima. Atua principalmente na área da voz.",
      "Funcao" => "Fonoaudiólogo",
      "Detalhes" => "/doutores?nome=daniel-lucas-picanco-marchand"
    ),
    9  => array (
      "Nome" => "Lucia Geyer",
      "Foto" => "3.jpg",
      "Descricao" => "Mestre em Saúde da Criança e do Adolescente pela UFRGS. Especialista em Audiologia pelo Conselho Federal de Fonoaudiologia (CFFa). Atualmente realiza avaliação audiológica de adultos e seleção e adaptação de aparelhos auditivos.",
      "Funcao" => "Fonoaudiólogo",
      "Detalhes" => "/doutores?nome=lucia-geyer"
    ),
    3  => array (
      "Nome" => "Dr. Airton Malinsky",
      "Foto" => "4.jpg",
      "Descricao" => "Preceptor de ensino de otorrinolaringologia do HCPA, chefe de serviço de otorrinolaringologia do Hospital de Pronto Socorro de Porto Alegre (HPS). Fundador da UNICRED e executa atividades de direção na UNIMED Porto Alegre.",
      "Funcao" => "Otorrinolaringologista",
      "Detalhes" => "/doutores?nome=airton-malinsky"
    ),
    5  => array (
      "Nome" => "Dra. Caroline Catherine Lacerda Elias",
      "Foto" => "10.jpg",
      "Descricao" => "Fellowship em Otologia no Hospital Mãe de Deus Porto Alegre. Médica Otologista do Hospital Nossa Senhora da Conceição. Atua principalmente na área de otologia, principalmente em otites crônicas.",
      "Funcao" => "Otorrinolaringologista",
      "Detalhes" => "/doutores?nome=caroline-catherine-lacerda-elias"
    ),
    12  => array (
      "Nome" => "Alana Signorini",
      "Foto" => "11.jpg",
      "Descricao" => "Mestre em Ciências Médicas da UFRGS. Especialista em Disfagia pelo Conselho Federal de Fonoaudiologia (CFFa). Atua nas áreas de reabilitação dos distúrbios da deglutição em crianças e adultos.",
      "Funcao" => "Fonoaudióloga",
      "Detalhes" => "/doutores?nome=alana-signorini"
    ),
    13  => array (
      "Nome" => "Letícia S. Abascal Bulcão",
      "Foto" => "12.jpg",
      "Descricao" => "Especialização em Terapia Sistêmica Individual, Conjugal e Familiar e formação em psicologia escolar. Atende crianças, adolescentes e adultos.<br>",
      "Funcao" => "Psicóloga",
      "Detalhes" => "/doutores?nome=leticia-abascal-bulcao"
    ),
  );
?>
<style type="text/css" media="screen">
  .item i {
      color: #fff !important;
      font-size: 12px !important;
      top: 30px !important;
      position: unset !important;
  }
  .linka {
      background-color: #9d71ae !important;
      color: #fff !important;
      transition: 500ms ease-in;
      font-size: 12px !important;
  }
</style>
<div class="main-content">
	<!-- BANNER -->
	<section class="inner-header divider layer-overlay overlay-deep" data-bg-img="/assets/img/bg-equipe.jpg">
		<div class="container pt-90 pb-50">
			<div class="section-content">
				<div class="row"> 
					<div class="col-md-12 xs-text-center pt-90">
						<h3 class="font-29 typo">Nossa equipe</h3>
						<ol class="breadcrumb white mt-10">
							<li>
								<a href="/">Home </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li>
								<a href="#"> Sobre nós </a>
							</li>
							&nbsp;<i class="far fa-circle" style="font-size: 6px;"></i>&nbsp;
							<li class="active text-theme-colored"> Nossa equipe</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="section-content" style="padding-top: 50px; padding-bottom: 50px;">
    <div class="container">
      <div class="row multi-row-clearfix">
        <?php
            for($i = 0; $i <= 1; $i++) {
        ?>
                <div class="col-sm-12 col-md-6 col-lg-6 mb-60 sm-text-center">
                  <div class="team maxwidth400">
                    <div class="thumb">
                      <img class="img-fullwidth" src="/assets/img/equipe/<?php echo $doutores[$i]['Foto']; ?>" alt="<?php echo $doutores[$i]['Nome']; ?>" title="<?php echo $doutores[$i]['Nome']; ?>">
                    </div>
                    <div class="content border-1px p-15 bg-white-light">
                      <h4 class="name mb-0 mt-0"><?php echo $doutores[$i]['Nome']; ?></h4>
                      <h6 class="title mt-0"><?php echo $doutores[$i]['Funcao']; ?></h6>
                      <p class="mb-30"><?php echo $doutores[$i]['Descricao']; ?></p>
                      <?php
                        if($i <= 1) {
                          echo '<a class="btn linka btn-sm pull-right flip" href="'.$doutores[$i]["Detalhes"].'">ver datalhes</a>';
                        }
                      ?>
                    </div>
                  </div>
                </div>
          <?php
            }
          ?>
          </div>
          <div class="row multi-row-clearfix">
          <?php
            for($i = 2; $i < count($doutores); $i++) {
          ?>    
              <div class="col-sm-12 col-md-6 col-lg-3 mb-60 sm-text-center">
                  <div class="team">
                    <div class="thumb">
                      <img class="img-fullwidth" src="/assets/img/equipe/<?php echo $doutores[$i]['Foto']; ?>" alt="<?php echo $doutores[$i]['Nome']; ?>" title="<?php echo $doutores[$i]['Nome']; ?>">
                    </div>
                    <div class="content border-1px p-15 bg-white-light">
                      <h4 class="name mb-0 mt-0"><?php echo $doutores[$i]['Nome']; ?></h4>
                      <h6 class="title mt-0"><?php echo $doutores[$i]['Funcao']; ?></h6>
                      <p class="mb-30"><?php echo $doutores[$i]['Descricao']; ?></p>
                      <?php
                        if($i <= 1) {
                          echo '<a class="btn linka btn-sm pull-right flip" href="'.$doutores[$i]["Detalhes"].'">ver datalhes</a>';
                        }
                      ?>
                    </div>
                  </div>
              </div>
          <?php
            }
          ?>
      </div>
    </div>
  </div>
</div>
<?php
	$notFound = false;
?>