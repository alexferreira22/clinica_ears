<?php

	/**
	*	URL AMIGAVEL
	*
	* 	Essa classe serve como rotas de url
	*
	*   @copyright Alexander da Silva Quadros Ferreira
	*	https://bitbucket.org/alexferreira22/
 	*/

	// RECEBO OS DADOS DO SERVIDDOR;
	// PULO A / (BARRA)
	// VERIFICO SE EXISTE A INTERROGAÇAO;
	// EXPLODE NA URL PARA SABER A POSICAO
 	// VERIFICO SE A URL[0] ESTA VAZIA, CASO SIM RETORNO A PAGINA PADRAO

	class Routes {
		private $url;

		public function getUrl() {
			return $this->url;
		} 

		public function setUrl($param) {
			if(!empty($param)):
				$param = trim(addslashes($param));

				$inBar = strpos($param, "/");
				if($inBar > -1):
					$param = substr($param, 1);
				endif;
				$inQuestion = strpos($param, "?");
				if($inQuestion > 0):
					$param = substr($param, 0, $inQuestion);
				endif;

				$this->url = $param;

			endif;
		}

		public function rota($url) {

			$url = explode("/", $url);

			$url[0] = ($url[0] != '' ? $url[0] : 'homepage');

			if(file_exists("app".DIRECTORY_SEPARATOR.$url[0].".php")):
				if(!isset($url[1])):
					return "app".DIRECTORY_SEPARATOR.$url[0].".php";
				else:
					if(isset($url[1]) && is_dir("app".DIRECTORY_SEPARATOR.$url[0].DIRECTORY_SEPARATOR.$url[1])):
						if(isset($url[2]) && file_exists("app".DIRECTORY_SEPARATOR.$url[0].DIRECTORY_SEPARATOR.$url[1].DIRECTORY_SEPARATOR.$url[2].".php")):
							return "app".DIRECTORY_SEPARATOR.$url[0].DIRECTORY_SEPARATOR.$url[1].DIRECTORY_SEPARATOR.$url[2].".php";
						else:
							return "app".DIRECTORY_SEPARATOR."404.php";
						endif;
					elseif(is_dir("app".DIRECTORY_SEPARATOR.$url[0])):
						if(isset($url[1]) && file_exists("app".DIRECTORY_SEPARATOR.$url[0].DIRECTORY_SEPARATOR.$url[1].".php")):
							return "app".DIRECTORY_SEPARATOR.$url[0].DIRECTORY_SEPARATOR.$url[1].".php";
						else:
							return "app".DIRECTORY_SEPARATOR."404.php";
						endif;
					else:
						return "app".DIRECTORY_SEPARATOR."404.php";
					endif;
				endif;
			else:
				return "app".DIRECTORY_SEPARATOR."404.php";
			endif;

		}
	}