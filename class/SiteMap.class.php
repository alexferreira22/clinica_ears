<?php

	/* SITEMAP */
	class SiteMap extends DataBase {
		private $url;

		public function __construct($dbname, $host, $user, $pass, $domain) {
			try {
				parent::__construct($dbname, $host, $user, $pass);
			} catch(PDOException $e) {
				die("Falha na conexão: ".$e->getMessage()." Linha: ".$e->getLine());
			}
			$this->url = $domain;
		}
	
		public function siteMapCreate() {
			$body = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; 
			$body .= '<url><loc>'.$this->url.'/</loc><changefreq>daily</changefreq><priority>1.00</priority></url>';
			$body .= '<url><loc>'.$this->url.'/painel/login</loc><changefreq>yearly</changefreq><priority>1.00</priority></url>';
			$pages = parent::selectConteudo("tb_cadastro_menu");
			$total = count($pages);
			$volta = 0;
			while($volta < $total) {
				if($pages[$volta]["Menu_Pai"] != '0') {
					$body .= '<url><loc>'.$this->url.'/'.strtolower($pages[$volta]["Endereco_Menu"]).'</loc><changefreq>daily</changefreq><priority>0.85</priority></url>';
				} else {
					$body .= '<url><loc>'.$this->url.'/'.strtolower($pages[$volta]["Endereco_Sub_Menu"]).'</loc><changefreq>daily</changefreq><priority>0.85</priority></url>';
				}
				$volta++;
			}
			$pages = parent::selectConteudo("tb_servicosConteudo");
			$total = count($pages);
			$volta = 0;
			while($volta < $total) {
				$body .= '<url><loc>'.$this->url.'/servicos?post='.strtolower($pages[$volta]["Endereco_Titulo"]).'</loc><changefreq>monthly</changefreq><priority>0.8</priority></url>';
				$volta++;
			}
			$pages = parent::selectConteudo("tb_categorias");
			$total = count($pages);
			$volta = 0;
			while($volta < $total) {
				$body .= '<url><loc>'.$this->url.'/blog?cat='.strtolower($pages[$volta]["Endereco_Cat"]).'</loc><changefreq>monthly</changefreq><priority>0.85</priority></url>';
				$volta++;
			}
			$pages = parent::selectConteudo("tb_blog");
			$total = count($pages);
			$volta = 0;
			while($volta < $total) {
				$body .= '<url><loc>'.$this->url.'/blog?post='.strtolower($pages[$volta]["Endereco_Titulo"]).'</loc><changefreq>monthly</changefreq><priority>0.8</priority></url>';
				$volta++;
			}
			$pages = parent::selectConteudo("tb_planos");
			$total = count($pages);
			$volta = 0;
			while($volta < $total) {
				$body .= '<url><loc>'.$this->url.'/planos?post='.strtolower($pages[$volta]["Endereco_Titulo"]).'</loc><changefreq>monthly</changefreq><priority>0.8</priority></url>';
				$volta++;
			}
			$body .= '</urlset>';
			$this->createXml($body);
		}

		public function createXml($code) {
			$path = "sitemap.xml";
			$handle = fopen($path, 'w+');
			fseek($handle, 0);
			fwrite($handle, $code);
			fclose($handle);
		}
		public function getUltimaExecucao(){

		    $data = parent::selectConteudo("tb_siteMap");

		    return DateTime::createFromFormat('Y-m-d', $data[0]["Data_Map"]);
		}

		public function verficaSiteMap() {
			$dataUltimaExecucao = $this->getUltimaExecucao();
			$dataAtual = new DateTime();
			if(!empty($dataUltimaExecucao)) {
				$diferenca = $dataAtual->diff($dataUltimaExecucao);
				$diferencaEmDias = $diferenca->format('%a');
				if ($diferencaEmDias >= 2){
				    $this->siteMapCreate();
				    parent::atualizarConteudo("tb_siteMap", array("Data_Map" => date('Y-m-d')), array("SiteMapID" => 1));
				}
			} else {
				$this->siteMapCreate();
				parent::insertConteudo("tb_siteMap", array("Data_Map"), array(date('Y-m-d')));
			}
		}
	}