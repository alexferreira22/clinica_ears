<?php
	
	// Define Set
	setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set("America/Sao_Paulo");

	set_time_limit(0);
	define("HOST", '127.0.0.1');
	define("IP", $_SERVER["REMOTE_ADDR"]);
	//define("ROOT", __DIR__);
	define("SERVER", $_SERVER["HTTP_HOST"]);
	//define("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"]);
	/*define("SERVER", $_SERVER["SCRIPT_FILENAME"]);*/
	define("URL","http" . (isset($_SERVER["HTTPS"]) ? (($_SERVER["HTTPS"]=="on") ? "s" : "") : "") . "://" . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
	//define("URLPadrao", "http" . (isset($_SERVER["HTTPS"]) ? (($_SERVER["HTTPS"]=="on") ? "s" : "") : "") . "://" . "$_SERVER[HTTP_HOST]");

	function autoLoad($nameClass) {
	    $extension =  spl_autoload_extensions();
		$dir = "class";

		try {
			if(file_exists($dir.DIRECTORY_SEPARATOR.$nameClass.$extension)) {
				require_once $dir.DIRECTORY_SEPARATOR.$nameClass.$extension;
			} else {
				throw new Exception("Erro ao incluir arquivo ".$nameClass.$extension, 400);
			}
		} catch(Exception $e) {
			die($e->getMessage()."<br>Código: ".$e->getCode());
		}
	}
	spl_autoload_extensions('.class.php');
	spl_autoload_register('autoLoad');
	
	$routes = new Routes();