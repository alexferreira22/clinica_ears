<?php

	/*
	* FUNCTION FILTRA GET e POST
	*
	*/
	function filtroGetPost($dados) {
		if(!empty($dados)) {
			
			$dados = htmlspecialchars($dados);
			$dados = preg_replace('/(|insert|select|delete|from|where|drop table|show tables|#|\*|\\\\)/','', $dados);
			$dados = addslashes($dados);

			return $dados;

		}
	}

	/*
	* 	REMOVER ACENTOS e CONVERTER PARA URL
	*	@param $string => Recebe a string e remove os caracteres especiais
	*/
	function toUrl($string, $slug = false) {
	  if(strpos($string, "&") > -1) {
	 	 	$string = str_replace('&', 'e', $string);
			$string = str_replace(array('amp', ';'), '', $string);
	  }
	  $string = strtolower($string);
	  // Código ASCII das vogais
	  $ascii['a'] = range(224, 230);
	  $ascii['e'] = range(232, 235);
	  $ascii['i'] = range(236, 239);
	  $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	  $ascii['u'] = range(249, 252);
	  // Código ASCII dos outros caracteres
	  $ascii['b'] = array(223);
	  $ascii['c'] = array(231);
	  $ascii['d'] = array(208);
	  $ascii['n'] = array(241);
	  $ascii['y'] = array(253, 255);
	  foreach ($ascii as $key=>$item) {
	    $acentos = '';
	    foreach ($item AS $codigo) $acentos .= chr($codigo);
	    $troca[$key] = '/['.$acentos.']/i';
	  }
	  $string = preg_replace(array_values($troca), array_keys($troca), $string);
	  // Slug?
	  if ($slug) {
	    // Troca tudo que não for letra ou número por um caractere ($slug)
	    $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
	    // Tira os caracteres ($slug) repetidos
	    $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
	    $string = trim($string, $slug);
	  }
	  $string = str_replace(array('(' , ')', ':', '?', '/', ','), '', $string);
	  $string = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
	  return $string;
	}
?>