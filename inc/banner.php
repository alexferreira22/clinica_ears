  <!-- Banner -->
  <div class="main-content">
    <section id="home" class="divider parallax fullscreen" data-stellar-background-ratio="0.5" data-bg-img="/assets/img/banner.jpg">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center">
                <div class="pb-50 pt-100">
                   <h3 class="mg-auto text-white font-30 inline-block pl-30 pr-30 mb-5 pt-5 pb-5">Clínica Ears</h3>
                  <h1 class="mg-auto text-white mt-0 inline-block pl-40 pr-40 pt-5 pb-5 font-42">Escute e Fale Melhor</h1>
                </div>
                  <div class="pt-60">
                    <a href="/contato" class="mg-auto btn pl-20 pr-20">Entre em contato</a>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
