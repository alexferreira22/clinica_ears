	<!-- RODAPE -->
	<?php
		if($notFound != true) {
	?>
	<footer id="footer" class="footer p-0 bg-black-111">
		<div class="container pb-20">
				<div class="row mb-50">
					<div class="footer-box-wrapper equal-height">
						<div class="col-sm-4 footer-box-one pr-0 pl-sm-0">
							<div class="footer-box icon-box media">
								<a href="/contato" class="media-left pull-left mr-30 mr-sm-15 text-white">
									<i class="far fa-envelope"></i>
								</a>
								<div class="media-body">
									<h4 class="media-heading heading title">Entrar em Contato</h4>
									<p><a href="/contato" class="text-white" target="_self" alt="Contato" title="Saiba mais">Mande sua mensagem.</a>
										<a href="/contato" target="_self" alt="Contato" title="Saiba mais"><i class="fa fa-arrow-circle-right font-14"></i>
											<br><br>
										</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 footer-box-two pl-0 pr-0">
							<div class="footer-box icon-box media">
								<a href="#" class="media-left pull-left mr-30 mr-sm-15 text-white" >
									<i class="far fa-clock"></i>
								</a>
								<div class="media-body">
									<h4 class="media-heading heading title">Horário de Atendimento</h4>
									<p>
										Segunda à Sexta das 8h às 20h.<br><br>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 footer-box-three pl-0 pr-sm-0">
							<div class="footer-box icon-box media">
								<a href="/ears-kids" class="media-left pull-left mr-30 mr-sm-15 text-white">
									<i class="fas fa-baby text-white"></i>
								</a>
								<div class="media-body">
									<h4 class="media-heading heading title">Espaço Kids</h4>
									<p><a href="/ears-kids" class="text-white" target="_self" alt="Espaço Kids" title="Saiba mais">Saiba mais</a>
										<a href="/ears-kids" target="_self" alt="Contato" title="Saiba mais">
											<i class="fa fa-arrow-circle-right font-14"></i>
										</a><br><br>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<div class="row multi-row-clearfix">
				<div class="col-sm-6 col-md-3">
					<div class="widget dark">
						<img alt="" src="/assets/img/ears-rodape.png">
						<p class="font-15 mt-20 mb-10 nexas">
							A EARS conta com profissionais altamente capacitadas e especializadas nas áreas de atendimento médico, diagnóstico, fonoterápico e psicológico.  
						</p>
						<a class="text-white font-15" href="/a-clinica" alt="A Clínica" title="Ler mais">
							<i class="fa fa-angle-double-right" style="color: #9b71ac !important;"></i> Ler mais
						</a>
						<ul class="social-icons icon-dark mt-20">
							<li>
								<a href="https://www.facebook.com/pages/EARS-Cl%C3%ADnica-De-Otorrinolaringologia-e-Fonoaudiologia/521078201598422" data-bg-color="#3B5998">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="widget dark">
						<h5 class="widget-title line-bottom typo font-19">Acesso rápido</h5>
						<ul class="list-border list theme-colored angle-double-right">
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								Sobre nós
								<ul class="dropdown">
									<a href="/a-clinica" title="Sobre a clínica!">A Clínica</a><br>
									<a href="/nossa-equipe" title="Sobre nossa equipe!">Nossa Equipe</a>
								</ul>
							</li>
							<li>
								<a href="/especialidades" title="Saiba mais sobre nossas especialidades!">Especialidades</a>
							</li>
							<li>
								<a href="/ears-kids" title="Espaço kids!">Ears Kids</a>
							</li>
							<li>
								<a href="/contato" title="Entre em contato conosco!">Contato</a>
							</li>

						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="widget dark">
						<h5 class="widget-title line-bottom line-bottom2 typo font-19">Especialidades</h5>
						<div class="latest-posts">
							<article class="post media-post clearfix pb-0 mb-10">
								<a class="post-thumb" href="/especialidades?servico=otorrinolaringologicos">
									<img src="/assets/img/img1.jpg" alt="Otorrinolaringologia" title="Otorrinolaringologia">
								</a>
								<div class="post-right">
									<h5 class="post-title mt-0 font-15 mb-5 text-white typo">
										<a href="/especialidades?servico=otorrinolaringologicos" class="text-white" target="_self">Otorrinolaringologia</a>
									</h5>
									<p class="post-date mb-02">
										<a class="font-12" href="/especialidades?servico=otorrinolaringologicos" target="_self" alt="Otorrinolaringologia" title="Saiba mais">
											Saiba mais <i class="fa fa-angle-double-right"></i>
										</a>
									</p>
								</div>
							</article>
							<article class="post media-post clearfix pb-0 mb-10">
								<a class="post-thumb" href="/especialidades?servico=fonoaudiologicos">
									<img src="/assets/img/img2.png" alt="Fonoaudiologia" title="Fonoaudiologia">
								</a>
								<div class="post-right">
									<h5 class="post-title mt-0 mb-5 font-15 text-white typo">
										<a href="/especialidades?servico=fonoaudiologicos" target="_self" class="text-white">Fonoaudiologia</a>
									</h5>
									<p class="post-date mb-02">
										<a class="font-12" href="/especialidades?servico=fonoaudiologicos" alt="Fonoaudiologia" title="Saiba mais">
											Saiba mais <i class="fa fa-angle-double-right"></i>
										</a>
									</p>
								</div>
							</article>
							<article class="post media-post clearfix pb-0 mb-10">
								<a class="post-thumb" href="/especialidades?servicos=psicologicos">
									<img src="/assets/img/img3.jpg" alt="Psicologia" title="Psicologia">
								</a>
								<div class="post-right">
									<h5 class="post-title mt-0 mb-5 font-15 text-white typo">
										<a href="/especialidades?servicos=psicologicos" target="_self" class="text-white">Psicologia</a></h5>
										<p class="post-date mb-02">
											<a class="font-12" href="/especialidades?servicos=psicologicos" alt="Otorrinolaringologia" title="Saiba mais">
												Saiba mais <i class="fa fa-angle-double-right"></i>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="widget dark">
							<h5 class="widget-title line-bottom typo font-19">Contato</h5>
							<ul class="list list-border">
								<li>
									<a href="tel:+555132074284" class="font-12">+(55) 51 3207-4284</a>
								</li>
								<li>
									<a href="mailto:ears@ears.com.br" class="font-12">ears@ears.com.br</a>
								</li>
								<li>
									<a href="#" class="lineheight-20 font-12">Rua Padre Chagas, 415, sala 501, Moinhos, Porto Alegre Rio Grande do Sul, Brasil</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-nav bg-black-222 p-20">
				<div class="row text-center">
					<div class="col-md-12">
						<p class="text-white m-0 typo font-15">Desenvolvido por <img src="/assets/img/Cachaca-Comunicacoes.png" title="Cachaça Comunicações" alt="Cachaça Comunicações"></p>
					</div>
				</div>
			</div>
		</footer>
		<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<?php
		}
	?>
<script src="/assets/js/custom.js"></script>
</body>
</html>