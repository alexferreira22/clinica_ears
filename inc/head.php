<?php
	switch ($_SERVER["REQUEST_URI"]) {
		case '/':
			$title = "Clínica Ears";
			$home = true;
		break;
		case '/a-clinica':
			$title = "A Clínica Ears";
			$sobre = true;
		break;
		case '/nossa-equipe':
			$title = "Nossa Equipe - Ears";
			$sobre = true;
		break;
		case '/especialidades':
			$title = "Especialidades - Ears";
			$especialidades = true;
		break;
		case '/ears-kids':
			$title = "Espaço Kids - Ears";
			$kids = true;
		break;
		case '/ears-kids':
			$title = "Espaço Kids - Ears";
		break;
		case '/contato':
			$title = "Contato - Ears";
			$contato = true;
		break;
		default:
			$title = "Clínica Ears";
			break;
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description""content="Descrição">
	<meta name="keywords" content="Ears, Clínica, Otorrinolaringologia, Fonoaudiologa">
	<meta name="author" content="Cachaça Comunicações">
	<meta name="publisher" content="http://cachacabr.com">
	<meta name="googlebot" content="index,noarchive,follow">
	<meta name="robots" content="index, follow">
	<meta name="msnbot" content="index,follow">
	<title><?php echo $title; ?></title>
	<link href="/assets/img/favicon.png" rel="shortcut icon" type="image/png">
	<!-- Stylesheet -->
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/animate.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/css-plugin-collections.css" rel="stylesheet"/>
	<!-- CSS | menuzord megamenu skins -->
	<link id="menuzord-menu-skins" href="/assets/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
	<!-- CSS | Main style file -->
	<link href="/assets/css/style-main.css" rel="stylesheet" type="text/css">
	<!-- CSS | Preloader Styles -->
	<link href="/assets/css/preloader.css" rel="stylesheet" type="text/css">
	<!-- CSS | Custom Margin Padding Collection -->
	<link href="/assets/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
		<!-- CSS | Responsive media queries -->
	<link href="/assets/css/responsive.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/assets/css/main.css">
	<link rel="stylesheet" href="/assets/css/fonts.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- external javascripts -->
	<script src="/assets/js/jquery-2.2.0.min.js"></script>
	<script src="/assets/js/jquery-ui.min.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<!-- JS | jquery plugin collection for this theme -->
	<script src="/assets/js/jquery-plugin-collection.js"></script>
	<script src="/assets/js/main.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<?php
		if($_SERVER["REQUEST_URI"] != '/') {
	?>
		<style type="text/css" media="screen">
			.header-nav.navbar-transparent.animated-active {
				background-color: #fff !important;
			}
			.menuzord-menu > li > a {
				color: #606060;
			}
			.logo-interna {
				display: block !important;
			}
			.logo-home {
				display: none;
			}
			.menuzord-menu > li > a:hover {
				color: #fff !important;
			}
			.menuzord .showhide em {
				background: #606060 !important;
			}
			.active a {
				color: #fff !important;
			}
			.dropdown > li > a {
				color: #606060 !important;
			}
			.dropdown > li > a:hover {
				color: #fff !important;
			}
			.header-nav.navbar-white {
				background-color: #fff !important;
			}
		</style>
	<?php
		}
		//----- HEADER -----//
		require_once "inc".DIRECTORY_SEPARATOR."header.php";
	?>