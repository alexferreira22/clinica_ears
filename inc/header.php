  <!--<div id="wrapper">
    <div id="preloader">
      <div id="spinner">
        <img src="/assets/img/ears-rodape.png" alt="">
      </div>
    </div>-->
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-nav navbar-fixed-top header-dark navbar-white navbar-transparent navbar-sticky-animated animated-active">
      <div class="header-nav-wrapper">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue">
            <a class="menuzord-brand pull-left flip" href="/">
              <img src="/assets/img/Clinica_Ears.png" alt="Clínica Ears" title="Clínica Ears" class="logo-home">
              <img src="/assets/img/Clinica_Ears2.png" alt="Clínica Ears" title="Clínica Ears" class="logo-interna">
            </a>
            <ul class="menuzord-menu">
              <li <?php echo (isset($home) && $home == true) ? 'class="active"' : '' ?>>
                <a href="/">Home</a>
              </li>
              <li <?php echo (isset($sobre) && $sobre == true) ? 'class="active"' : '' ?>>
                <a href="#">Sobre nós</a>
                <ul class="dropdown">
                    <li>
                      <a href="/a-clinica" title="Sobre a clínica!">A Clínica</a>
                    </li>
                    <li>
                      <a href="/nossa-equipe" title="Sobre nossa equipe!">Nossa Equipe</a>
                    </li>
                  </ul>
              </li>
              <li <?php echo (isset($especialidades) && $especialidades == true) ? 'class="active"' : '' ?>>
                <a href="/especialidades" title="Saiba mais sobre nossas especialidades!">Especialidades</a>
              </li>
              <li <?php echo (isset($kids) && $kids == true) ? 'class="active"' : '' ?>>
                <a href="/ears-kids" title="Espaço kids!">Ears Kids</a>
              </li>
              <li <?php echo (isset($contato) && $contato == true) ? 'class="active"' : '' ?>>
                <a href="/contato" title="Entre em contato conosco!">Contato</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
  
  <?php
      if($_SERVER["REQUEST_URI"] == "/") {
        //----- BANNER -----//
        require_once "inc".DIRECTORY_SEPARATOR."banner.php";
      }
  ?>