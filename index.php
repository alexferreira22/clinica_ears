<?php
	//----- INICIANDO A SESSAO -----//
	session_start();

	//----- REQUIRE -----//
	require_once 'config'.DIRECTORY_SEPARATOR.'config.php';
	require_once 'config'.DIRECTORY_SEPARATOR.'functions.php';
	$request = filter_input(INPUT_SERVER, 'REQUEST_URI');

	//----- SET URL E ATRIBUINDO O VALOR DA URL NA VARIAVEL -----//
	$routes->setUrl($request);
	$url = $routes->getUrl();

	//----- HEAD -----//
	require_once "inc".DIRECTORY_SEPARATOR."head.php";

	//----- RETORNANDO A ROTA -----//
	require_once $routes->rota($url);

	//----- FOOTER -----//
	require_once "inc".DIRECTORY_SEPARATOR."footer.php";
?>